# kotogif

## small project that saves random cat gif from [randomcatgifs.com](https://randomcatgifs.com/)

### usage in [other readme](cli/kotogif/readme.md)

## installation:

### Dependecies:

Just install ffmpeg and add it to "path" variable.



### From releases:

Just go to [releases page](https://codeberg.org/hrueschwein/kotogif/releases) and get package for your OS.
Note that there may be not the latest version

### Building from source:
1) You need have Go installed
2) make `go install codeberg.org/hrueschwein/kotogif/cli/kotogif@latest`
